<?php
namespace Bx\Service;
use Bx\Facade\Facade;
use Bx\Assert\Assert;
use Bitrix\Main\EventManager;
use Closure;

/**
 * Абстрактный сервис.
 */
abstract class Service
{
    use Facade;

    /**
     * Выполняется при инициализации сервиса.
     */
    abstract protected function boot();



    /**
     * Возвращает обработчик события по его имени.
     * @param  string   $handlerName Имя обработчика событий.
     * @return callable              Обработчик событий.
     */
    private function getEventHandler(string $handlerName) : callable
    {
        $callable = [$this, $handlerName];

        if (method_exists(Closure::class, 'fromCallable'))
        {
            $handler = Closure::fromCallable($callable);
            return $handler;
        }

        return function(...$args) use ($callable)
        {
            $result = call_user_func_array($callable, $args);
            return $result;
        };
    }

    /**
     * Возвращает параметры события - имя и модуль.
     * @param  string $eventName Событие.
     * @param  string &$module   Модуль события.
     * @param  string &$name     Имя события.
     * @return array             Параметры события.
     */
    private function getEventParts(string $eventName, string &$module, string &$name)
    {
        $eventParts = explode('.', $eventName, 2);
        $eventPartsCount = count($eventParts);

        $module = $eventPartsCount > 1 ? $eventParts[0] : 'main';
        $name = $eventPartsCount > 1 ? $eventParts[1] : $eventParts[0];
    }

    /**
     * Добавляет обработчик указанному событию.
     * @param  string $eventName   Имя события.
     * @param  string $handlerName Имя метода-обработчика.
     * @return void
     */
    protected function bind(string $eventName, string $handlerName)
    {
        Assert::notEmpty($eventName);
        Assert::notEmpty($handlerName);
        Assert::methodExists($this, $handlerName);

        $module = $name = '';
        $event = $this->getEventParts($eventName, $module, $name);
        $handler = $this->getEventHandler($handlerName);

        $manager = EventManager::getInstance();
        $manager->addEventHandler($module, $name, $handler);
    }



    /**
     * True, если плагин инициализирован.
     * @var bool
     */
    private $isInited = false;

    /**
     * Инициализирует сервис в "php_interface/init.php".
     * @return void
     */
    public static function init()
    {
        $instance = self::getInstance();

        if ($instance->isInited)
        {
            return;
        }

        $instance->isInited = true;
        $instance->boot();
    }
}
